/*
ID: raincro1
LANG: JAVA
TASK: namenum
*/
import java.io.*;
import java.util.*;

class namenum {
    static class IO {
        BufferedReader          f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = false;
    static String           name  = "namenum";

    static IO               io;

    public static void main (String [] args) throws IOException {
        io = new IO();

        final String num_line = io.nextLine();

        final int num_length = num_line.length();

        if(num_line.indexOf('0') >= 0 || num_line.indexOf('1') >= 0){
            io.out.println("NONE");
            io.close();
        }

        // here we assume that the string consists of only [2-9] chars

        final int[] digits = new int [num_length];

        for(int i = 0; i < num_length; i++) {
            digits[i] = Integer.parseInt("" + num_line.charAt(i));
        }


        final BufferedReader dict_reader = new BufferedReader(new FileReader("dict.txt"));

        final String[] groups = {"ABC", "DEF", "GHI", "JKL", "MNO", "PRS", "TUV", "WXY"};   // + 2

        final char last_first = groups[digits[0] - 2].charAt(2);

        String word;
        int total = 0;

        while((word = dict_reader.readLine()) != null){

            System.out.println(word);

            debug = word.indexOf("KR") == 0;

            if(word.charAt(0) > last_first) break;

            if(debug) System.out.println(word + ": 1");

            if(word.length() != num_length) continue;

            if(debug) System.out.println(word + ": 2");

            boolean clean = true;

            for(int i = 0; i < num_length; i++) {
                String gr = groups[digits[i] - 2];

                if(gr.indexOf(word.charAt(i)) < 0) {
                    if(debug) System.out.println(word + " dirty at " + i);
                    clean = false;
                    break;
                }
            }

            if(clean){
                io.out.println(word);
                total++;
            }
        }

        if(total == 0){
            io.out.println("NONE");
        }

        // io.out.printf("%d %d\n", max1, max2);
        io.close();
    }
}
