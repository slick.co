/*
ID: raincro1
LANG: JAVA
TASK: transform
*/
import java.io.*;
import java.util.*;

class transform {
    static class IO {
        BufferedReader          f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = true;
    static String           name  = "transform";

    static IO               io;

    static boolean[] get_pattern(int dim) throws IOException {
        boolean[] pat = new boolean[dim * dim];

        for(int y = 0; y < dim; y++){
            String line = io.nextLine();

            for(int x = 0; x < dim; x++){
                pat[y * dim + x] = line.charAt(x) == '@';
            }
        }

        /*
        for(int y = 0; y < dim; y++){
            for(int x = 0; x < dim; x++){
                System.out.print(pat[y * dim + x] ? '+' : '-');
            }

            System.out.println();
        }
        */

        return pat;
    }

    static void report_result(int result) {
        io.out.println(result);
        io.close();
    }

    public static void main (String [] args) throws IOException {
        io = new IO();

        final int dim = io.nextIntLine();

        final boolean[]   pat1 = get_pattern(dim);
        final boolean[]   pat2 = get_pattern(dim);

        class Trans {
            boolean     sw;
            boolean     xinv;
            boolean     yinv;

            int         result;

            public Trans (int _sw, int _xi, int _yi, int _res) {
                sw      = _sw != 0;
                xinv    = _xi != 0;
                yinv    = _yi != 0;
                result  = _res;
            }

            public boolean pat2_cell(int x, int y){
                int xx = xinv ? dim - x - 1 : x;
                int yy = yinv ? dim - y - 1 : y;

                return pat2[ sw ? yy + xx * dim : yy * dim  + xx];
            }

            public void report(){
                report_result(result);
            }
        }

        LinkedList<Trans>   trs = new LinkedList<Trans> ();

        trs.add(new Trans(1, 0, 1, 1));
        trs.add(new Trans(0, 1, 1, 2));
        trs.add(new Trans(1, 1, 0, 3));
        trs.add(new Trans(0, 1, 0, 4));
        trs.add(new Trans(1, 1, 1, 5));
        trs.add(new Trans(0, 0, 1, 5));
        trs.add(new Trans(1, 0, 0, 5));
        trs.add(new Trans(0, 0, 0, 6));

        for(int y = 0; y < dim; y++){
            for(int x = 0; x < dim; x++){
                // System.out.printf("%d,%d\n", x, y);

                if(trs.size() == 0){
                    report_result(7);
                }

                ListIterator<Trans> it = trs.listIterator();

                while(it.hasNext()){
                    Trans   t = it.next();

                    if(pat1[y * dim + x] != t.pat2_cell(x, y)){
                        it.remove();
                    }
                }
            }
        }

        if(trs.size() == 0){
            report_result(7);
        }else{
            trs.getFirst().report();
        }


        // io.out.printf("%d %d\n", max1, max2);
        io.close();
    }
}
