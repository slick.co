/*
ID: raincro1
LANG: JAVA
TASK: milk2
*/
import java.io.*;
import java.util.*;

class milk2 {
    static class IO {
        BufferedReader          f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static class Pair {
        public  int         a, b;

        Pair(int _a, int _b) { a = _a; b = _b; }
    }

    static class PairComparator implements Comparator<Pair> {
        public int compare (Pair x, Pair y){
            return x.a < y.a ? -1 : x.a > y.a ? +1 : 0;
        }
    }

    static boolean          debug = true;
    static String           name  = "milk2";

    static IO               io;

    public static void main (String [] args) throws IOException {
        io = new IO();

        int count = io.nextIntLine();

        Pair[]      pairs = new Pair[count];

        for(int i = 0; i < count; i++){
            io.nextLine();

            int     s = io.nextIntToken();
            int     e = io.nextIntToken();

            pairs[i] = new Pair(s, e);
        }

        Arrays.sort(pairs, new PairComparator());

        int     max1 = 0;
        int     max2 = 0;
        int     s = pairs[0].a;
        int     e = s;

        for(Pair p : pairs){
            if(e < p.a){
                int d = e - s;
                if(max1 < d) max1 = d;

                d = p.a - e;
                if(max2 < d) max2 = d;

                s = p.a;
            }
            
            if(e < p.b) e = p.b;
        }

        {
            int d = e - s;
            if(max1 < d) max1 = d;
        }

        io.out.printf("%d %d\n", max1, max2);
        io.close();
    }
}
