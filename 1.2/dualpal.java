/*
ID: raincro1
LANG: JAVA
TASK: dualpal
*/
import java.io.*;
import java.util.*;

class dualpal {
    static class IO {
        BufferedReader          f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static class CoolNumber {
        int     base;
        int     max_digit;

        int[]   digits;

        public CoolNumber (int _num, int _base){
            base = _base;

            digits = new int [17];

            for(max_digit = 0; _num > 0; max_digit++){
                digits[max_digit] = _num % base;
                _num /= base;
            }
        }

        public void add(CoolNumber other){
            boolean carry = false;

            for(int i = 0; (i < max_digit && i < other.max_digit) || carry; i++){
                int sum = digits[i] + other.digits[i] + (carry ? 1 : 0);
                
                carry = (sum >= base);

                if(carry) sum -= base;

                digits[i] = sum;

                if(max_digit <= i) max_digit = i + 1;
            }
        }

        public void increment(){
            boolean carry = true;

            for(int i = 0; carry; i++){
                int sum = digits[i] + 1;

                carry = (sum >= base);

                if(carry) sum = 0;

                digits[i] = sum;

                if(max_digit <= i) max_digit = i + 1;
            }
        }

        public boolean isPalindrome() {
            for(int i = max_digit / 2 - 1; i >= 0; i--){
                if(digits[i] != digits[max_digit - i - 1]) return false;
            }

            return true;
        }

        public String toString() {
            final char[] chars = "0123456789ABCDEFGHIJ".toCharArray();

            StringBuilder sb = new StringBuilder(max_digit);

            // sb.append(max_digit);
            // sb.append(":");

            for(int i = max_digit - 1; i >= 0; i--){
                sb.append(chars[digits[i]]);
            }

            return sb.toString();
        }
    }

    static boolean          debug = false;
    static String           name  = "dualpal";

    static IO               io;

    public static void main (String [] args) throws IOException {
        io = new IO();

        io.nextLine();

        int num   = io.nextIntToken();
        int start = io.nextIntToken();

        final CoolNumber[] nums = new CoolNumber [9];

        for(int i = 0; i < 9; i++){
            nums[i] = new CoolNumber(start, i + 2);
        }

        while(num > 0){

            start++;

            int count = 0;
            for(int i = 0; i < 9; i++){
                nums[i].increment();

                if(nums[i].isPalindrome()) count++;
            }

            if(count >= 2){
                num--;

                io.out.println(start);
            }
        }

        // io.out.printf("%d %d\n", max1, max2);
        io.close();
    }
}
