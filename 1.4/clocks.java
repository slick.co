/*
ID: raincro1
LANG: JAVA
TASK: clocks
*/
import java.io.*;
import java.util.*;

class clocks {
    static class IO {
        public BufferedReader   f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = false;
    static String           name  = "clocks";

    static IO               io;

    //////////////////////////////////////////

    static final byte[][]    moves = {
        { 0, 1, 3, 4 },
        { 0, 1, 2 },
        { 1, 2, 4, 5 },
        { 0, 3, 6 },
        { 1, 3, 4, 5, 7 },
        { 2, 5, 8 },
        { 3, 4, 6, 7 },
        { 6, 7, 8 },
        { 4, 5, 7, 8 }
    };

    static State        state_pool = null;

    static class Move {
        public  int      val;
        public  Move     pre;

        public Move(Move pre, int val) {
            this.val = val;
            this.pre = pre;
        }

        public void print() {
            if(pre == null) {
                io.out.print(val + 1);
            }else{
                pre.print();
                io.out.printf(" %d", val + 1);
            }
        }

        public void sys_print() {
            if(pre == null) {
                System.out.print(val + 1);
            }else{
                pre.sys_print();
                System.out.printf(" %d", val + 1);
            }
        }
    }

    static class State {
        public  Move    move;
        public  int     iter;           // move iteration
        public  int[]   state = null;

        public  State   next = null;    // chain pointer

        public State(Move move, int iter) {
            this.move = move;
            this.iter = iter;
        }

        void check() {
            for(int i = 0; i < 9; i++){
                if(state[i] != 0) return;
            }

            move.print();

            io.out.println();
            io.close();
        }
    }

    static State genState(Move move, int mid, int iter, State first, State last) {
        State   st;

        if(state_pool != null){
            st = state_pool;
            state_pool = st.next;
            st.next = null;

            st.move = new Move(move, mid);

            st.iter = iter;

            for(int i = 0; i < 9; i++){
                st.state[i] = first.state[i];
            }
        }else{
            st = new State(new Move(move, mid), iter);
            st.state = first.state.clone();
        }

        // apply move
        for(int i = moves[mid].length - 1; i >= 0; i--){
            final int id = moves[mid][i];
            st.state[id] = (st.state[id] + 1) % 4;
        }

        st.check();

        last.next = st;     // don't forget to append the state

        return st;
    }

    static State poolState(State st){
        State n = st.next;

        st.next = state_pool;
        state_pool = st;

        return n;
    }

    public static void main (String [] args) throws IOException {
        io = new IO();

        State   first, last;

        first = last = new State(null, 0);

        first.state = new int [9];

        for(int i = 0; i < 9; i++){
            if(i % 3 == 0) io.nextLine();
            first.state[i] = io.nextIntToken() / 3 % 4;
        }

        first.check();

        for(int id = 0; id < 9; id++){
            last = genState(null, id, 0, first, last);
        }

        first = first.next;

        for(int iii = 0; iii < 1000000; iii++){
            final Move  move = first.move;
            final int   mid = move.val;
            final int   iter = first.iter;

            if(iter < 2){
                last = genState(move, mid, iter + 1, first, last);
            }

            for(int id = mid + 1; id < 9; id++){
                last = genState(move, id, 0, first, last);
            }

            first = poolState(first);
        }

        // io.out.println(min_area);
        // io.close();
    }
}


