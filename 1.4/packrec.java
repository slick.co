/*
ID: raincro1
LANG: JAVA
TASK: packrec
*/
import java.io.*;
import java.util.*;

class packrec {
    static class IO {
        public BufferedReader   f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = false;
    static String           name  = "packrec";

    static IO               io;

    //////////////////////////////////////////

    static int                          min_area;
    static TreeMap<Integer, Integer>    min_map;

    static class Rect {
        int     x, y;

        public Rect (int x, int y) {
            this.x = x;
            this.y = y;
        }

        Rect growRight(Rect peer){
            return new Rect(this.x + peer.x, this.y > peer.y ? this.y : peer.y);
        }

        Rect growDown(Rect peer) {
            return new Rect(this.x > peer.x ? this.x : peer.x, this.y + peer.y);
        }

        boolean isSmall() {
            return x * y < min_area;        // search tree pruning
        }
        
        void checkResult() {
            int area = x * y;

            if(min_area > area) {
                min_area = area;
                min_map.clear();
            }

            if(min_area == area) {
                if(x <= y)  min_map.put(x, y);
                else        min_map.put(y, x);
            }
        }
    }

    static void checkLast(Rect r1, Rect r2, Rect r3, Rect r4){
        // check layout 6

        //
        //   1  4
        //   2  3
        //

        int mx = r1.x;
        if(mx < r2.x) mx = r2.x;

        int b;

        if(r2.y > r3.y){
            if(r2.y >= r3.y + r4.y){
                return;         // this is equivalent to layout 3
            }

            mx += r4.x;

            b = r2.x + r3.x;
        }else{
            if(r1.y + r2.y <= r3.y){
                return;         // this is equivalent to layout 3
            }

            mx += r3.x;

            b = r1.x + r4.x;
        }

        if(mx < b) mx = b;

        int my = r1.y + r2.y;
        int c = r3.y + r4.y;
        if(my < c) my = c;

        (new Rect(mx, my)).checkResult();
    }

    static void checkOther(Rect r1, Rect r2, Rect r3, Rect r4){
        final Rect block = r1.growRight(r2);

        if(block.isSmall()){
            block.growDown(r3).growRight(r4).checkResult();     // layout 3
            block.growRight(r3.growDown(r4)).checkResult();     // layout 5
        }

        checkLast(r1, r2, r3, r4);
        checkLast(r2, r1, r3, r4);
    }

    static void iterate3rd(Rect r1, Rect r2, Rect r3, Rect r4){
        final Rect block = r1.growRight(r2).growRight(r3);

        if(block.isSmall()){
            block.growRight(r4).checkResult();                  // layout 1
            block.growDown(r4).checkResult();                   // layout 2
        }

        // iterate 3rd parameter
        checkOther(r1, r2, r3, r4);
        checkOther(r1, r3, r2, r4);
        checkOther(r2, r3, r1, r4);
    }

    static void iterate4th(
        int x1, int y1, int x2, int y2,
        int x3, int y3, int x4, int y4)
    {
        Rect    r1 = new Rect(x1, y1);
        Rect    r2 = new Rect(x2, y2);
        Rect    r3 = new Rect(x3, y3);
        Rect    r4 = new Rect(x4, y4);

        // iterate 4th parameter
        iterate3rd(r1, r2, r3, r4);
        iterate3rd(r1, r2, r4, r3);     // NOTE: checkOther performs the same blockcheck twice, but we dont care for now
        iterate3rd(r1, r3, r4, r2);
        iterate3rd(r2, r3, r4, r1);
    }

    public static void main (String [] args) throws IOException {
        io = new IO();

        io.nextLine();
        final int x1 = io.nextIntToken();
        final int y1 = io.nextIntToken();

        io.nextLine();
        final int x2 = io.nextIntToken();
        final int y2 = io.nextIntToken();

        io.nextLine();
        final int x3 = io.nextIntToken();
        final int y3 = io.nextIntToken();

        io.nextLine();
        final int x4 = io.nextIntToken();
        final int y4 = io.nextIntToken();

        min_area = (x1 + x2 + x3 + x4) * (y1 + y2 + y3 + y4) * 2;

        min_map = new TreeMap<Integer, Integer> ();

        // iterate all transpositions
        iterate4th(x1, y1, x2, y2, x3, y3, x4, y4);
        iterate4th(x1, y1, x2, y2, x3, y3, y4, x4);

        iterate4th(x1, y1, x2, y2, y3, x3, x4, y4);
        iterate4th(x1, y1, x2, y2, y3, x3, y4, x4);


        iterate4th(x1, y1, y2, x2, x3, y3, x4, y4);
        iterate4th(x1, y1, y2, x2, x3, y3, y4, x4);
                               
        iterate4th(x1, y1, y2, x2, y3, x3, x4, y4);
        iterate4th(x1, y1, y2, x2, y3, x3, y4, x4);


        iterate4th(y1, x1, x2, y2, x3, y3, x4, y4);
        iterate4th(y1, x1, x2, y2, x3, y3, y4, x4);
                       
        iterate4th(y1, x1, x2, y2, y3, x3, x4, y4);
        iterate4th(y1, x1, x2, y2, y3, x3, y4, x4);
                       
                       
        iterate4th(y1, x1, y2, x2, x3, y3, x4, y4);
        iterate4th(y1, x1, y2, x2, x3, y3, y4, x4);
                               
        iterate4th(y1, x1, y2, x2, y3, x3, x4, y4);
        iterate4th(y1, x1, y2, x2, y3, x3, y4, x4);


        io.out.println(min_area);

        Map.Entry<Integer, Integer>     entry;

        while((entry = min_map.pollFirstEntry()) != null) {
            io.out.printf("%d %d\n", entry.getKey(), entry.getValue());
        }

        io.close();
    }
}


