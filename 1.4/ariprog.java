/*
ID: raincro1
LANG: JAVA
TASK: ariprog
*/
import java.io.*;
import java.util.*;

class ariprog {
    static class IO {
        public BufferedReader   f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = false;
    static String           name  = "ariprog";

    static IO               io;

    //////////////////////////////////////////

    static int                  progression_limit = 0;

    static Vector<Integer>      bisqs = null;
    static BitSet               bisq_set = null;

    static final TreeMap<Integer, Vector<Integer>>    result = new TreeMap<Integer, Vector<Integer>> ();

    static void register_new_progression(final int last_value, final int delta) {
        // System.out.printf("Registered %d %d\n", last_value - delta * (progression_limit - 1), delta);

        final int first = last_value - delta * (progression_limit - 1);

        Vector<Integer> item = result.get(delta);
        if(item == null) item = new Vector<Integer> (10);

        item.add(first);

        result.put(delta, item);
    }

    static boolean bisquare_process_inner (int prev, final int delta) {
        for(int j = 2; j < progression_limit; j++){
            final int pp = prev - delta;
            if(pp < 0 || !bisq_set.get(pp)) return false;
            prev = pp;
        }

        return true;
    }

    static void bisquare_process (final int value) {
        for(int i = bisqs.size() - 1; i >= progression_limit - 2; i--) {
            final int   prev = bisqs.get(i);
            final int   delta = value - prev;

            if(bisquare_process_inner(prev, delta)) {
                register_new_progression(value, delta);
            }
        }

        bisqs.add(value);
        bisq_set.set(value);
    }

    static int bisquare_search(final int limit) {
        class Cell implements Comparable<Cell> {
            public int      x, y;
            public int      val;

            public Cell(int x, int y) {
                this.set(x, y);
            }

            public void set(int x, int y) {
                this.x = x;
                this.y = y;
                this.val = x * x + y * y;
            }

            public int compareTo (Cell peer) {
                if(this.val < peer.val) return -1;
                if(this.val > peer.val) return +1;
                return 0;
            }
        }

        class CellPool {
            final Stack<Cell>   cell_pool = new Stack<Cell> ();

            Cell alloc(int x, int y) {
                if(cell_pool.empty()) {
                    return new Cell(x, y);
                }

                Cell c = cell_pool.pop();
                c.set(x, y);
                return c;
            }

            void release(Cell c) {
                cell_pool.push(c);
            }

        }

        final PriorityQueue<Cell>       pri = new PriorityQueue<Cell> ();

        final CellPool                  cell_pool = new CellPool();

        pri.add(new Cell(0, 0));

        int     prev_value = -1;
        Cell    cell = null;

        int     total = 0;

        while((cell = pri.poll()) != null) {
            final int val = cell.val;

            if(prev_value != val) {
                assert(prev_value < val);

                prev_value = val;

                bisquare_process(val);  // <--

                // System.out.printf("%d ", val);
                total++;
            }

            final int x = cell.x;
            final int y = cell.y;

            // System.out.printf("Entered: %d %d %d\n", x, y, val);

            if(x == y + 1){
                pri.add(cell_pool.alloc(x, y + 1));
            }

            if(x < limit){
                pri.add(cell_pool.alloc(x + 1, y));
            }

            cell_pool.release(cell);
        }

        return total;

        // System.out.println();
        // System.out.printf("Total: %d", total);
    }

    public static void main (String [] args) throws IOException {
        io = new IO();

        progression_limit = io.nextIntLine();

        final int slimit = io.nextIntLine();
        // final int slimit = 5;

        bisqs = new Vector<Integer> (80 * slimit * slimit / 100, 1024);
        bisq_set = new BitSet (2 * slimit * slimit);

        // try {
            bisquare_search(slimit);
        // } catch (OutOfMemoryError e) {
        //     System.err.println("OutOfMemoryError exception");
        //     // System.err.printf("DiffRecord allocated: %d\n", DiffRecord.total_alloced);
        //     System.exit(1);
        // }

        if(result.size() > 0){
            Map.Entry<Integer, Vector<Integer>>     entry;
            while((entry = result.pollFirstEntry()) != null) {
                final int delta = entry.getKey();

                final Vector<Integer> firsts = entry.getValue();

                for(Integer i : firsts) {
                    io.out.printf("%d %d\n", i, delta);
                }

            }
        }else{
            io.out.println("NONE");
        }

        /*
        // count the percentage of distinct cells (limits to 67%)
        for(int i = 1; i <= 250; i++){
            final int check = bisquare_search(i);
            final int total = (i + 2) * (i + 1) / 2;

            System.out.printf("%d: %5d %5d %02d\n", i, check, total, 100 * check / total);
        }
        */



        // io.out.println(min_area);
        io.close();
    }
}


