/*
ID: raincro1
LANG: JAVA
TASK: gift1
*/
import java.io.*;
import java.util.*;

class gift1 {
    static class IO {
        BufferedReader          f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static class Account {
        public  int v = 0;
    }

    static boolean          debug = true;
    static String           name  = "gift1";

    static IO               io;

    public static void main (String [] args) throws IOException {
        io = new IO();

        String[]                names;
        Map<String, Account>    accs = new HashMap<String, Account>();

        int         NP = io.nextIntLine();

        // read names
        names = new String[NP];

        for(int i = 0; i < NP; i++){
            String n = new String(io.nextLine());
            names[i] = n;

            accs.put(n, new Account());
        }

        // read giving groups
        for(int i = 0; i < NP; i++){
            String giver = new String(io.nextLine());

            io.nextLine();

            int     amount = io.nextIntToken();
            int     peer_count = io.nextIntToken();

            if(peer_count > 0){
                int     fee = amount / peer_count;

                accs.get(giver).v += amount % peer_count;
                accs.get(giver).v -= amount;

                for(int j = 0; j < peer_count; j++){
                    String  peer = io.nextLine();
                    accs.get(peer).v += fee;
                }
            }
        }

        for(String n : names){
            io.out.printf("%s %d\n", n, accs.get(n).v);
        }

        io.close();                                  // close the output file
    }
}
