/*
ID: raincro1
LANG: JAVA
TASK: ride
*/
import java.io.*;
import java.util.*;

class ride {
    static boolean          debug = false;
    static String           name  = "ride";

    static BufferedReader   f;
    static PrintWriter      out;

    static int chsum(String s){
        String  ch = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        int     sum = 1;

        for(int i = 0; i < s.length(); i++){
            sum *= ch.indexOf(s.charAt(i) + 1);
        }

        return sum % 47;
    }

    public static void main (String [] args) throws IOException {
        if(debug) {
            f = new BufferedReader(new InputStreamReader(System.in));
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
        } else {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        // StringTokenizer st = new StringTokenizer(f.readLine());

        String      s1, s2;

        if(debug){
            s1 = "COMETQ";
            s2 = "HVNGAT";
            s1 = "ABSTAR";
            s2 = "USACO";
        } else {
            s1 = f.readLine();
            s2 = f.readLine();
        }

        out.println( (chsum(s1) == chsum(s2)) ? "GO" : "STAY" );

        out.close();                                  // close the output file
        System.exit(0);                               // don't omit this!
    }
}
