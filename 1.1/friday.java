/*
ID: raincro1
LANG: JAVA
TASK: friday
*/
import java.io.*;
import java.util.*;

class friday {
    static class IO {
        BufferedReader          f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = true;
    static String           name  = "friday";

    static IO               io;

    public static void main (String [] args) throws IOException {
        io = new IO();

        int[]       std  = { 3, 0, 3, 2, 3, 2, 3, 3, 2, 3, 2, 3 };
        int[]       leap = { 3, 1, 3, 2, 3, 2, 3, 3, 2, 3, 2, 3 };

        int         total = io.nextIntLine();

        int[]       stat = { 0, 0, 0, 0, 0, 0, 0 };

        int         cur = 0;

        for(int i = 0; i < total; i++){
            int[]   off = (i % 4 != 0) ? std : (i % 100 != 0) ? leap : (i % 400 != 100) ? std : leap;

            // System.out.printf("%d %s: %d\n", i, off == leap ? "+" : "-", cur);

            for(int j = 0; j < 12; j++){
                stat[cur]++;

                cur = (cur + off[j]) % 7;
            }
        }

        for(int i = 0; i < 6; i++){
            io.out.printf("%d ", stat[i]);
        }
        io.out.println(stat[6]);

        io.close();                                  // close the output file
    }
}
