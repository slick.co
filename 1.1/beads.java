/*
ID: raincro1
LANG: JAVA
TASK: beads
*/
import java.io.*;
import java.util.*;

class beads {
    static class IO {
        BufferedReader          f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = true;
    static String           name  = "beads";

    static IO               io;

    public static void main (String [] args) throws IOException {
        io = new IO();

        final int   total_len = io.nextIntLine();

        class Scanner {
            char[]      ch;
            int         aux = 0;
            int         i = 0;
            int         max = 0;

            Scanner (String s) { ch = s.toCharArray(); }

            public char next() {
                if(i >= total_len * 2) {

                    if(max <= 1) max = total_len;
                    else if(max > total_len) max = total_len;

                    io.out.println(max);
                    io.close();
                }

                return ch[i++];
            }

            public void set_aux(){
                aux = i;
                if(aux > total_len) aux = total_len;

                System.out.printf("Extended to %d\n", aux);
            }

            public void extend(int candidate) {
                if(max < candidate) {
                    max = candidate;

                    System.out.printf("Max -> %d\n", max);
                }
            }

            public int index() { return i; }
        }

        class LineScanner {
            public Scanner  scan;
            public char     nextChar;

            LineScanner(String s) {
                scan = new Scanner(s);
            }

            public int countNext(){
                int count = 0;
                char next;
                do {
                    count++;
                    next = scan.next();
                } while(nextChar == next);

                nextChar = next;

                return count;
            }
        }

        LineScanner     scan;

        {
            String str = io.nextLine();
            scan = new LineScanner(str + str);              // note the line duplication!
        }

        int         prevprev = 0;
        int         prev = 0;
        int         whites = 0;
        char        prevChar = 'q';

        boolean     flag1 = true;
        boolean     flag2 = true;

        scan.nextChar = 'q';

        for(;;){
            do{
                /*
                if(flag1 && prevChar != 'q') {
                    if(flag2){
                        flag2 = false;
                    }else{
                        scan.scan.set_aux();                 // extend the scanning length
                        flag1 = false;
                    }
                }
                */

                System.out.printf("%3s: to %s->%s: %d %d %d\n", scan.scan.index(), prevChar, scan.nextChar, prevprev, prev, whites);

                scan.scan.extend(prevprev + prev + whites);

                prevprev = prev;
                prevChar = scan.nextChar;
                prev = whites + scan.countNext();

                whites = 0;
            } while(scan.nextChar != 'w');

            for(;;){
                whites = scan.countNext();

                if(scan.nextChar == prevChar){
                    prev += whites;

                    prev += scan.countNext();

                    if(scan.nextChar == 'w'){
                        continue;
                    }

                    whites = 0;
                }

                break;
            }
        }

        /*
        io.out.println("OH, SHI...");           // that should never happen
        io.close();
        */
    }
}
