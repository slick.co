/*
ID: raincro1
LANG: JAVA
TASK: calfflac
*/
import java.io.*;
import java.util.*;

class calfflac {
    static class IO {
        public BufferedReader   f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = false;
    static String           name  = "calfflac";

    static IO               io;

    public static void main (String [] args) throws IOException {
        io = new IO();

        class EOFException extends Throwable {
        }

        class Internal {
            IO      io;

            int     index;
            char[]  buffer;
            int     buffer_len;

            Internal(IO _io) throws IOException {
                io = _io;

                buffer = new char[20000];
                buffer_len = io.f.read(buffer, 0, 20000);
                index = 0;
            }

            char next_char () throws EOFException {
                for(;;){
                    if(index >= buffer_len){
                        throw new EOFException();
                    }

                    char c = Character.toUpperCase(buffer[index++]);

                    if(Character.getType(c) == Character.UPPERCASE_LETTER) {
                        return c;
                    }
                }
            }

            int cur_index () {
                return index - 1;
            }

            String substr(int first, int last){
                return new String(buffer, first, last - first + 1);
            }
        }

        final char[]   chars = new char[20000];
        final short[]  indices = new short[20000];

        class Extern {
            Internal        intr;

            public int      char_count;

            Extern(IO _io) throws IOException {
                intr = new Internal(_io);

                char_count = 0;
            }

            char next_char () throws EOFException {
                char ch = intr.next_char();

                chars[char_count] = ch;
                indices[char_count] = (short)intr.cur_index();

                char_count++;

                return ch;
            }

            String substr(int first, int last){
                return intr.substr(first, last);
            }
        }

        final Extern    extr = new Extern(io);

        // these two describe the max palindrom found by now
        int     first = 0;
        int     length = 0;

        // tails describe tails search pointers
        //      (stored pointers to the elements to be checked)
        final LinkedList<Integer>   tails = new LinkedList<Integer> ();

        try {
            extr.next_char();

            length = 1;     // we found at least one char!

            for(;;){
                char ch = extr.next_char();

                ListIterator<Integer>   it = tails.listIterator();

                while(it.hasNext()){
                    int tail = it.next();

                    if(tail <= 0 || chars[tail - 1] != ch){
                        it.remove();

                        int nu_len = extr.char_count - tail - 1;

                        if(nu_len > length){
                            first = tail;
                            length = nu_len;
                        }
                    }else{
                        it.set(tail - 1);
                    }
                }

                // spawn tails
                tails.add(extr.char_count - 1);

                // check for even palindrom
                if(chars[extr.char_count - 2] == chars[extr.char_count - 1]){
                    tails.add(extr.char_count - 2);
                }
            }
        } catch (EOFException e) {
            for(int tail : tails){
                int nu_len = extr.char_count - tail;

                if(nu_len > length){
                    first = tail;
                    length = nu_len;
                }
            }

        }


        // io.out.printf("%d %d\n", first, length);
        io.out.println(length);
        io.out.println(extr.substr(indices[first], indices[first + length - 1]));


        // io.out.printf("%d %d\n", max1, max2);
        io.close();
    }
}
