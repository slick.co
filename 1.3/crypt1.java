/*
ID: raincro1
LANG: JAVA
TASK: crypt1
*/
import java.io.*;
import java.util.*;

class crypt1 {
    static class IO {
        public BufferedReader   f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = false;
    static String           name  = "crypt1";

    static IO               io;

    static class N3 {       // digit holder
        public int d0 = 0;
        public int d1 = 0;
        public int d2 = 0;
    };

    public static void main (String [] args) throws IOException {
        io = new IO();

        //
        // fill digit_map, digit_list, and digit_count
        //

        final boolean[] digit_map = {
            false, false, false, false, false, 
            false, false, false, false, false 
        };

        final int[] digit_list = new int[9];

        int digit_count = 0;

        {
            int count = io.nextIntLine();

            io.nextLine();

            for(int i = 0; i < count; i++){
                int d = io.nextIntToken();

                digit_map[d] = true;
            }

            digit_count = 0;

            for(int i = 1; i < 10; i++){            // NOTE: 1
                if(digit_map[i]) digit_list[digit_count++] = i;
            }

            if(digit_count <= 0) {
                io.out.println(0);
                io.close();
            }
        }


        int                 base = 0;        // base number -- current 1st muplier
        int                 base_delta      = digit_list[0] * 111;

        N3                  base_n = new N3 ();         // base number indices

        final int[]         muls = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        final boolean[]     muls_marks = { false, false, false, false, false, false, false, false, false };
        final int[]         muls_list = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        final N3[]          muls_digits = {
            new N3 (), new N3 (), new N3 (), 
            new N3 (), new N3 (), new N3 (), 
            new N3 (), new N3 (), new N3 () 
        };

        int                 muls_list_count = 0;

        int                 solution_count = 0;

        // limits

        int                 m_limit = digit_count;        // muls limit

        for(;;){
            //
            // apply delta and mark muls
            //

            base += base_delta;

            System.out.printf("Base: %d\n", base);

            muls_list_count = 0;

            for(int i = 0; i < m_limit; i++){
                muls[i] += base_delta * digit_list[i];
                muls_marks[i] = false;

                int m = muls[i];

                if(m > 999){
                    m_limit = i;
                    break;
                }

                int d;

                // decompose the mul and check it's digits
                d = m % 10;

                if( !digit_map[d] ) continue;

                muls_digits[i].d0 = d;

                m /= 10;
                d = m % 10;

                if( !digit_map[d] ) continue;

                muls_digits[i].d1 = d;
                m /= 10;
                d = m;

                if( !digit_map[d] ) continue;

                muls_digits[i].d2 = d;

                // mark this mul as valid (i.e. the mul contains only the allowed digits)
                muls_marks[i] = true;
                muls_list[muls_list_count++] = i;
            }

            //
            // we take all the valid muls and double-iterate
            // over them checking the summation result
            //

            for(int j = 0; j < muls_list_count; j++){
                final N3        m2 = muls_digits[muls_list[j]];

                for(int i = 0; i < muls_list_count; i++){
                    final N3    m1 = muls_digits[muls_list[i]];

                    // System.out.printf("%d %d %d +++ %d %d %d\n", m2.d2, m2.d1, m2.d0, m1.d2, m1.d1, m1.d0);

                    int         sum;
                    boolean     carry;

                    sum = m1.d1 + m2.d0;
                    carry = sum >= 10;
                    if(carry) sum -= 10;

                    if( !digit_map[sum] ) continue;

                    sum = m1.d2 + m2.d1 + (carry ? 1 : 0);
                    carry = sum >= 10;
                    if(carry) sum -= 10;

                    if( !digit_map[sum] ) continue;

                    if( carry && (m2.d2 == 9 || !digit_map[m2.d2 + 1]) ) continue;

                    solution_count++;           // register a solution
                }
            }

            //
            // advance to the next base number
            //

            if((++base_n.d0) >= digit_count) {
                base_n.d0 = 0;
                if((++base_n.d1) >= digit_count){
                    base_n.d1 = 0;
                    if((++base_n.d2) >= digit_count){
                        break;
                    }
                }
            }

            int base_new = digit_list[base_n.d2] * 100 + digit_list[base_n.d1] * 10 + digit_list[base_n.d0];

            base_delta = base_new - base;
        }

        System.out.println(solution_count);
        io.out.println(solution_count);

        // io.out.printf("%d %d\n", max1, max2);
        io.close();
    }
}
