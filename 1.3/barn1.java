/*
ID: raincro1
LANG: JAVA
TASK: barn1
*/
import java.io.*;
import java.util.*;

class barn1 {
    static class IO {
        BufferedReader          f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = false;
    static String           name  = "barn1";

    static IO               io;

    public static void main (String [] args) throws IOException {
        io = new IO();

        io.nextLine();

        final int board_count   = io.nextIntToken();
        final int stall_count   = io.nextIntToken();
        final int count         = io.nextIntToken();

        final int[] cows = new int [count];

        for(int i = 0; i < count; i++){
            cows[i] = io.nextIntLine();
        }

        Arrays.sort(cows);

        final int range = cows[count - 1] - cows[0] + 1;

        int result = 0;

        if(board_count >= count) {
            result = count;
        }else{
            for(int i = count - 1; i >= 1; i--){
                cows[i] -= cows[i - 1] + 1;
            }
            cows[0] = 0;

            Arrays.sort(cows);

            /*
            for(int i = 0; i < count; i++){
                System.out.printf("%d ", cows[i]);
            }
            System.out.println();
            */

            for(int i = count - board_count + 1; i < count; i++){
                result += cows[i];
            }

            result = range - result;
        }

        io.out.println(result);

        // io.out.printf("%d %d\n", max1, max2);
        io.close();
    }
}
