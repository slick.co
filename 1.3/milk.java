/*
ID: raincro1
LANG: JAVA
TASK: milk
*/
import java.io.*;
import java.util.*;

class milk {
    static class IO {
        BufferedReader          f;
        StringTokenizer         st;
        public PrintWriter      out;

        public String           line;

        public IO () throws IOException {
            f = new BufferedReader(new FileReader(name + ".in"));
            out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        }

        public String nextLine() throws IOException {
            line = f.readLine();
            st = null;
            return line;
        }

        public int nextIntLine() throws IOException {
            return Integer.parseInt(nextLine());
        }

        public void tokenize() {
            st = new StringTokenizer(line);
        }

        public String nextToken() {
            if(st == null) tokenize();
            return st.nextToken();
        }

        public int nextIntToken() {
            return Integer.parseInt(nextToken());
        }

        public void close() {
            out.close();                                  // close the output file
            System.exit(0);                               // don't omit this!
        }
    }

    static boolean          debug = false;
    static String           name  = "milk";

    static IO               io;

    public static void main (String [] args) throws IOException {
        io = new IO();

        io.nextLine();

        int total = io.nextIntToken();
        int count = io.nextIntToken();

        final TreeMap<Integer, Integer>     tr = new TreeMap<Integer, Integer> ();

        for(int i = 0; i < count; i++){
            io.nextLine();

            int price = io.nextIntToken();
            int amount = io.nextIntToken();

            Integer prev = tr.put(price, amount);
            if(prev != null){
                tr.put(price, amount + prev);
            }
        }

        int sum = 0;

        Map.Entry<Integer, Integer>      entry = tr.pollFirstEntry();

        while(entry != null){
            int price = entry.getKey();
            int amount = entry.getValue();

            System.out.printf("Polled: %d %d\n", price, amount);

            if(total <= amount){
                sum += price * total;
                break;
            }else{
                sum += price * amount;
                total -= amount;
            }

            entry = tr.pollFirstEntry();
        }

        io.out.println(sum);
        // io.out.printf("%d %d\n", max1, max2);
        io.close();
    }
}
