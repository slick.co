SUBDIR=1.4

subdir : usaco-bundle
	$(MAKE) -C $(SUBDIR)

clean : usaco-bundle
	$(MAKE) -C $(SUBDIR) clean

usaco-bundle : .git/HEAD .git/refs/heads/master
	git bundle create usaco-bundle --all
